import pytest
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession

from apps.auth.services import AuthService, TokenService
from apps.users.services import UserService
from dependencies import get_crypt_context
from settings import AuthSettings


@pytest.fixture
def crypt_context() -> CryptContext:
    return get_crypt_context()


@pytest.fixture
def token_service(
    auth_settings: AuthSettings,
    session: AsyncSession,
) -> TokenService:
    return TokenService(auth_settings=auth_settings, session=session)


@pytest.fixture
def auth_service(
    session: AsyncSession, token_service: TokenService, crypt_context: CryptContext
) -> AuthService:
    return AuthService(
        session=session,
        token_service=token_service,
        crypt_context=crypt_context,
    )


@pytest.fixture
def user_service(session: AsyncSession, crypt_context: CryptContext) -> UserService:
    return UserService(
        session=session,
        crypt_context=crypt_context,
    )
