import asyncio
import os
import sys
import uuid
from asyncio import AbstractEventLoop
from typing import AsyncIterable, Iterable

import dotenv
import httpx
import hypothesis
import pytest
from common.settings import get_settings
from fastapi import FastAPI
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from apps.auth._commands import REFRESH_TOKEN_COOKIE_NAME
from db.models import User
from dependencies import get_crypt_context
from settings import AppSettings, AuthSettings

dotenv.load_dotenv(".env")
hypothesis.settings.register_profile("default", max_examples=25)
hypothesis.settings.register_profile("ci", max_examples=1000)
hypothesis.settings.load_profile(os.getenv("HYPOTHESIS_PROFILE", "default"))

pytest_plugins = [
    "conftest_db",
    "conftest_services",
]


@pytest.fixture(scope="session")
def fastapi_app() -> FastAPI:
    from app import create_app

    return create_app()


@pytest.fixture
def endpoint_url(fastapi_app: FastAPI, operation_id: str) -> str:
    return fastapi_app.url_path_for(operation_id)


@pytest.fixture(scope="session")
def http_domain() -> str:
    return "127.0.0.1"


@pytest.fixture(scope="session")
async def http_client(
    fastapi_app: FastAPI,
    http_domain: str,
) -> AsyncIterable[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=fastapi_app,
        base_url=f"http://{http_domain}",
    ) as client:
        yield client


@pytest.fixture(scope="session")
def event_loop() -> Iterable[AbstractEventLoop]:
    if sys.platform.startswith("win"):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    policy = asyncio.get_event_loop_policy()
    loop = policy.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def crypt_context() -> CryptContext:
    return get_crypt_context()


@pytest.fixture
async def user_password() -> str:
    return str(uuid.uuid4())


@pytest.fixture(scope="session")
def auth_settings() -> AuthSettings:
    return get_settings(AuthSettings)


@pytest.fixture(scope="session")
def app_settings() -> AppSettings:
    return get_settings(AppSettings)


@pytest.fixture
async def user(
    session: AsyncSession,
    user_password: str,
    crypt_context: CryptContext,
) -> User:
    user = User(
        username=str(uuid.uuid4()),
        password_hash=crypt_context.hash(user_password),
    )
    session.add(user)
    await session.flush()
    await session.refresh(user)
    return user


@pytest.fixture
async def _user_tokens(
    user: User,
    user_password: str,
    fastapi_app: FastAPI,
    http_domain: str,
) -> tuple[str, str]:
    async with httpx.AsyncClient(
        app=fastapi_app, base_url=f"http://{http_domain}"
    ) as client:
        response = await client.post(
            "/auth/token",
            data={
                "username": user.username,
                "password": user_password,
                "grant_type": "password",
                "scopes": ["basic"],
            },
        )
    assert response.status_code == status.HTTP_200_OK
    access_token = response.json()["access_token"]
    refresh_token = response.cookies[REFRESH_TOKEN_COOKIE_NAME]
    return access_token, refresh_token


@pytest.fixture
async def user_access_token(_user_tokens: tuple[str, str]) -> str:
    return _user_tokens[0]


@pytest.fixture
async def user_refresh_token(_user_tokens: tuple[str, str]) -> str:
    return _user_tokens[1]
