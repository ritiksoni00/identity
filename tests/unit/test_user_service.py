import uuid

import pytest
from passlib.context import CryptContext

from apps.users.dto import UserCreateDto
from apps.users.exceptions import UserAlreadyExists
from apps.users.services import UserService


async def test_create_user(
    user_service: UserService,
    crypt_context: CryptContext,
) -> None:
    username = str(uuid.uuid4())
    password = str(uuid.uuid4())

    user = await user_service.create_user(
        dto=UserCreateDto(
            username=username,
            password=password,
        )
    )
    assert user.username == username
    assert user.password_hash != password
    assert crypt_context.verify(
        secret=password,
        hash=user.password_hash,
    )


async def test_raises_error_on_duplicate_username(
    user_service: UserService,
) -> None:
    username = str(uuid.uuid4())
    await user_service.create_user(
        dto=UserCreateDto(
            username=username,
            password="",
        )
    )
    with pytest.raises(UserAlreadyExists):
        await user_service.create_user(
            dto=UserCreateDto(
                username=username,
                password="",
            )
        )
