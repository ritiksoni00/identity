import hypothesis
from hypothesis import HealthCheck, strategies
from hypothesis.strategies import characters

from apps.auth.services import AuthService
from db.models import User


@hypothesis.settings(
    suppress_health_check=[HealthCheck.function_scoped_fixture],
)
@hypothesis.given(
    username=strategies.text(),
    password=strategies.text(),
)
async def test_authenticate_no_user(
    username: str,
    password: str,
    auth_service: AuthService,
) -> None:
    user = await auth_service.authenticate(
        username=username,
        password=password,
    )
    assert user is None


@hypothesis.settings(
    suppress_health_check=[HealthCheck.function_scoped_fixture],
)
@hypothesis.given(
    password=strategies.text(alphabet=characters(blacklist_characters=["\x00"])),
)
async def test_invalid_password(
    user: User,
    password: str,
    auth_service: AuthService,
) -> None:
    authenticated_user = await auth_service.authenticate(
        username=user.username,
        password=password,
    )
    assert authenticated_user is None


async def test_valid_user(
    user: User,
    user_password: str,
    auth_service: AuthService,
) -> None:
    authenticated_user = await auth_service.authenticate(
        username=user.username,
        password=user_password,
    )
    assert user is authenticated_user
