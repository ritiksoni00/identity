import uuid
from datetime import datetime, timedelta, timezone

import httpx
import jwt
import pytest
from common.tests import ApproxDateTime
from fastapi import status
from sqlalchemy.ext.asyncio import AsyncSession

from apps.auth._commands import REFRESH_TOKEN_COOKIE_NAME
from db.models import RefreshToken, User
from settings import AuthSettings


@pytest.fixture
def operation_id() -> str:
    return "auth_refresh"


async def test_no_token(http_client: httpx.AsyncClient, endpoint_url: str) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


async def test_invalid_token(http_client: httpx.AsyncClient, endpoint_url: str) -> None:
    response = await http_client.post(
        endpoint_url, cookies={REFRESH_TOKEN_COOKIE_NAME: str(uuid.uuid4())}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


async def test_expired_token(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    user: User,
    session: AsyncSession,
    auth_settings: AuthSettings,
) -> None:
    refresh_token = RefreshToken(
        user=user,
        scopes=tuple(),
        valid_until=datetime.now(tz=timezone.utc) - timedelta(seconds=1),
    )
    session.add(refresh_token)
    await session.flush()
    await session.refresh(refresh_token)

    response = await http_client.post(
        endpoint_url, cookies={REFRESH_TOKEN_COOKIE_NAME: str(refresh_token.id)}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.parametrize(
    "scopes",
    [
        tuple(),
        ("basic",),
    ],
)
async def test_base_case(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    user: User,
    session: AsyncSession,
    scopes: tuple[str],
    auth_settings: AuthSettings,
) -> None:
    refresh_token = RefreshToken(
        user=user,
        scopes=scopes,
        valid_until=datetime.now(tz=timezone.utc) + timedelta(seconds=1),
    )
    session.add(refresh_token)
    await session.flush()
    await session.refresh(refresh_token)

    response = await http_client.post(
        endpoint_url, cookies={REFRESH_TOKEN_COOKIE_NAME: str(refresh_token.id)}
    )
    assert response.status_code == status.HTTP_200_OK

    access_token = response.json()["access_token"]
    claims = jwt.decode(
        access_token,
        algorithms=[auth_settings.jwt_algorithm],
        key=auth_settings.jwt_public_key,
    )
    assert tuple(claims["scopes"]) == scopes


async def test_refresh_token_sliding_window(
    user_refresh_token: str,
    user_access_token: str,
    user: User,
    session: AsyncSession,
    http_client: httpx.AsyncClient,
    auth_settings: AuthSettings,
) -> None:
    now = datetime.now(tz=timezone.utc)
    refresh_token_model: RefreshToken = await session.get(
        RefreshToken, user_refresh_token
    )
    refresh_token_model.valid_until = now + timedelta(minutes=1)
    session.expunge(refresh_token_model)
    assert refresh_token_model

    response = await http_client.post(
        "/auth/refresh",
        cookies={REFRESH_TOKEN_COOKIE_NAME: user_refresh_token},
    )
    assert response.status_code == status.HTTP_200_OK

    new_refresh_token_model: RefreshToken = await session.get(
        RefreshToken, user_refresh_token
    )
    assert new_refresh_token_model.valid_until == ApproxDateTime(
        datetime.now(tz=timezone.utc) + auth_settings.refresh_token_lifetime,
        abs=timedelta(seconds=1),
    )
