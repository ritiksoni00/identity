import uuid

import httpx
import hypothesis
import pytest
from fastapi import FastAPI
from hypothesis import strategies
from starlette import status

from db.models import User


@pytest.fixture(scope="session")
def operation_id() -> str:
    return "users_retrieve"


@hypothesis.given(user_id=strategies.uuids())
async def test_not_found(
    http_client: httpx.AsyncClient,
    user_id: uuid.UUID,
    operation_id: str,
    fastapi_app: FastAPI,
) -> None:
    endpoint_url = fastapi_app.url_path_for(operation_id, user_id=user_id)
    response = await http_client.get(endpoint_url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


async def test_base_case(
    http_client: httpx.AsyncClient,
    user: User,
    fastapi_app: FastAPI,
    operation_id: str,
) -> None:
    endpoint_url = fastapi_app.url_path_for(operation_id, user_id=str(user.id))
    response = await http_client.get(endpoint_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "id": str(user.id),
        "username": user.username,
        "createdAt": user.created_at.isoformat(),
    }
