import strawberry

from gql.context import Info
from gql.users.types import UserType


@strawberry.type
class UsersQuery:
    @strawberry.field
    async def get_user(
        self,
        id: strawberry.ID,
        info: Info,
    ) -> UserType | None:
        return await info.context.loaders.user_by_id.load(id)

    @strawberry.field
    async def me(self, info: Info) -> UserType | None:
        user = info.context.maybe_user
        if not user:
            return None
        return await info.context.loaders.user_by_id.load(user.sub)
