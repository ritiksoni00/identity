import uuid
from typing import Annotated

from aioinject import Inject
from aioinject.ext.strawberry import inject
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import User


@inject
async def load_user_by_id(
    user_ids: list[uuid.UUID | str], session: Annotated[AsyncSession, Inject]
) -> list[User | None]:
    stmt = select(User).where(User.id.in_(user_ids))
    users = {str(user.id): user for user in await session.scalars(stmt)}
    return [users.get(str(user_id)) for user_id in user_ids]
