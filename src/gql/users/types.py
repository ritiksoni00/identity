from __future__ import annotations

import datetime

import strawberry.federation

from gql.context import Info


@strawberry.federation.type(
    name="User",
    keys=("id",),
)
class UserType:
    id: strawberry.ID
    username: str
    created_at: datetime.datetime

    @classmethod
    async def resolve_reference(cls, id: strawberry.ID, info: Info) -> UserType | None:
        user = await info.context.loaders.user_by_id.load(id)
        if user is None:
            return None

        return UserType(
            id=strawberry.ID(str(user.id)),
            username=user.username,
            created_at=user.created_at,
        )
