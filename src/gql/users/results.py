import strawberry

from gql.users.types import UserType


@strawberry.type
class UserCreateResult:
    user: UserType | None = None
