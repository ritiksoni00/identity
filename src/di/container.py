from functools import lru_cache
from typing import Iterable, Type

import aioinject
from common.settings import settings_getter
from pydantic import BaseSettings
from sqlalchemy.ext.asyncio import AsyncSession

from apps.auth._commands import CreateTokensCommand, RefreshTokensCommand
from apps.auth.services import AuthService, TokenService
from apps.users._commands import UserCreateCommand, UserRetrieveCommand
from apps.users.services import UserService
from db.dependencies import get_session
from dependencies import get_crypt_context
from settings import AuthSettings, DatabaseSettings


@lru_cache
def create_container() -> aioinject.Container:
    container = aioinject.Container()
    container.register(aioinject.Callable(get_session, type_=AsyncSession))
    container.register(aioinject.Callable(AuthService))
    container.register(aioinject.Callable(UserService))
    container.register(aioinject.Callable(TokenService))
    container.register(aioinject.Callable(get_crypt_context))

    container.register(aioinject.Callable(CreateTokensCommand))
    container.register(aioinject.Callable(RefreshTokensCommand))
    container.register(aioinject.Callable(UserCreateCommand))
    container.register(aioinject.Callable(UserRetrieveCommand))

    settings: Iterable[Type[BaseSettings]] = (AuthSettings, DatabaseSettings)
    for cls in settings:
        provider = aioinject.Singleton(settings_getter(cls), type_=cls)
        container.register(provider)

    return container
