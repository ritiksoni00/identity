from typing import Literal

from pydantic import BaseModel


class Tokens(BaseModel):
    access_token: str
    token_type: Literal["Bearer"] = "Bearer"
    expires_in: int
