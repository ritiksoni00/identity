from datetime import datetime, timezone
from typing import Any

import jwt
from common.auth.models import AuthTokenPayload
from jwt import PyJWTError
from passlib.context import CryptContext
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from db.models import RefreshToken
from db.models.users import User
from settings import AuthSettings
from types_ import SequenceOrTuple

from .schema import Tokens


class TokenService:
    def __init__(
        self,
        session: AsyncSession,
        auth_settings: AuthSettings,
    ) -> None:
        self._settings: AuthSettings = auth_settings
        self._session = session

    async def generate_access_token(
        self, user: User, scopes: SequenceOrTuple[str]
    ) -> Tokens:
        now = datetime.now()
        payload = AuthTokenPayload(
            sub=str(user.id),
            iat=int(now.timestamp()),
            exp=int(
                now.timestamp() + self._settings.access_token_lifetime.total_seconds()
            ),
            username=user.username,
            scopes=scopes,
            permissions=[],
        )

        token = jwt.encode(
            payload.dict(),
            key=self._settings.jwt_private_key,
            algorithm=self._settings.jwt_algorithm,
        )

        return Tokens(
            access_token=token,
            expires_in=int(self._settings.access_token_lifetime.total_seconds()),
        )

    async def decode_jwt(self, token: str) -> dict[str, Any]:
        return jwt.decode(
            token,
            key=self._settings.jwt_public_key,
            algorithms=[self._settings.jwt_algorithm],
        )

    async def generate_refresh_token(
        self, user: User, scopes: SequenceOrTuple[str]
    ) -> RefreshToken:
        token = RefreshToken(
            user=user,
            scopes=scopes,
            valid_until=datetime.now(tz=timezone.utc)
            + self._settings.refresh_token_lifetime,
        )
        self._session.add(token)
        await self._session.flush()
        await self._session.refresh(token)
        return token

    async def find_refresh_token(self, token: str) -> RefreshToken | None:
        stmt = (
            select(RefreshToken)
            .options(joinedload(RefreshToken.user))
            .where(
                RefreshToken.id == token,
                RefreshToken.valid_until >= datetime.now(tz=timezone.utc),
            )
        )
        token_model: RefreshToken | None = await self._session.scalar(stmt)
        return token_model

    async def renew_refresh_token(
        self,
        refresh_token: RefreshToken,
    ) -> RefreshToken:
        now = datetime.now(tz=timezone.utc)
        if now >= refresh_token.valid_until:
            raise ValueError
        refresh_token.valid_until = now + self._settings.refresh_token_lifetime
        return refresh_token


class AuthService:
    def __init__(
        self,
        session: AsyncSession,
        crypt_context: CryptContext,
        token_service: TokenService,
    ):
        self._session = session
        self._crypt_context = crypt_context
        self._token_service = token_service

    async def authenticate(
        self,
        username: str,
        password: str,
    ) -> User | None:
        user: User | None = await self._session.scalar(
            select(User).where(User.username == username)
        )
        if not user:
            return None

        verified, new_hash = self._crypt_context.verify_and_update(
            secret=password,
            hash=user.password_hash,
        )
        if not verified:
            return None

        if new_hash:
            user.password_hash = new_hash
            self._session.add(user)
            await self._session.flush()
            await self._session.refresh(user)

        return user

    async def authenticate_jwt(self, token: str) -> User | None:
        try:
            payload = await self._token_service.decode_jwt(token=token)
        except PyJWTError:
            return None

        user: User | None = await self._session.get(User, ident=payload["sub"])
        return user
