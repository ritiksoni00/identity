import datetime

from common.web.exceptions import BadRequest, Unauthorized
from fastapi.responses import Response
from fastapi.security import OAuth2PasswordRequestFormStrict
from starlette.requests import Request

from apps.auth.schema import Tokens
from apps.auth.services import AuthService, TokenService
from settings import AuthSettings

REFRESH_TOKEN_COOKIE_NAME = "__Host-refresh-token"


class CreateTokensCommand:
    def __init__(
        self,
        auth_service: AuthService,
        token_service: TokenService,
        auth_settings: AuthSettings,
    ):
        self._auth_service = auth_service
        self._token_service = token_service
        self._auth_settings = auth_settings

    async def execute(
        self,
        form: OAuth2PasswordRequestFormStrict,
        response: Response,
    ) -> Tokens:
        user = await self._auth_service.authenticate(
            username=form.username,
            password=form.password,
        )
        if user is None:
            raise BadRequest

        tokens = await self._token_service.generate_access_token(
            user=user,
            scopes=form.scopes,
        )
        refresh_token = await self._token_service.generate_refresh_token(
            user=user, scopes=form.scopes
        )
        response.set_cookie(
            key=REFRESH_TOKEN_COOKIE_NAME,
            value=str(refresh_token.id),
            secure=True,
            httponly=True,
            samesite="Strict",
            max_age=int(datetime.timedelta(days=30).total_seconds()),
        )
        return tokens


class RefreshTokensCommand:
    def __init__(
        self,
        token_service: TokenService,
    ):
        self._token_service = token_service

    async def execute(self, request: Request) -> Tokens:
        refresh_token_str = request.cookies.get(REFRESH_TOKEN_COOKIE_NAME)
        if not refresh_token_str:
            raise Unauthorized

        refresh_token = await self._token_service.find_refresh_token(
            token=refresh_token_str
        )
        if not refresh_token:
            raise Unauthorized

        await self._token_service.renew_refresh_token(refresh_token)
        tokens = await self._token_service.generate_access_token(
            user=refresh_token.user,
            scopes=refresh_token.scopes,
        )
        return tokens
