from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import Depends, HTTPException
from starlette import status

from db.models import User

from .oauth_scheme import oauth2_scheme
from .services import AuthService


@inject
async def get_current_user(
    auth_service: Annotated[AuthService, Inject],
    token: str = Depends(oauth2_scheme),
) -> User:
    user = await auth_service.authenticate_jwt(token=token)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    return user
