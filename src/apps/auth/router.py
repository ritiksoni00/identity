from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from common.web.exceptions import APIException
from fastapi import APIRouter, Depends
from fastapi.requests import Request
from fastapi.responses import Response
from fastapi.security import OAuth2PasswordRequestFormStrict

from ._commands import CreateTokensCommand, RefreshTokensCommand
from .schema import Tokens

router = APIRouter()


@router.post(
    "/token",
    response_model=Tokens,
)
@inject
async def auth_token(
    response: Response,
    command: Annotated[CreateTokensCommand, Inject],
    form: OAuth2PasswordRequestFormStrict = Depends(),
) -> Tokens:
    return await command.execute(
        form=form,
        response=response,
    )


@router.post(
    "/refresh",
    responses={
        "200": {"model": Tokens},
        "401": {"model": APIException.Schema},
    },
)
@inject
async def auth_refresh(
    request: Request,
    command: Annotated[RefreshTokensCommand, Inject],
) -> Tokens:
    return await command.execute(
        request=request,
    )
