import uuid
from typing import Annotated

import pydantic
from aioinject import Inject
from aioinject.ext.fastapi import inject
from common.schema.users import UserSchema
from common.web.exceptions import APIException
from fastapi import APIRouter, Depends, Query
from pydantic import conset
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from apps.auth.dependencies import get_current_user
from apps.users._commands import UserCreateCommand
from apps.users.schema import UserCreateSchema
from db.models import User
from dependencies import entity_getter

router = APIRouter()


@router.post(
    "",
    responses={
        status.HTTP_201_CREATED: {"model": UserSchema},
        status.HTTP_400_BAD_REQUEST: {"model": APIException.Schema},
    },
    status_code=status.HTTP_201_CREATED,
)
@inject
async def users_create(
    user_schema: UserCreateSchema,
    user_create_command: Annotated[UserCreateCommand, Inject],
) -> UserSchema:
    return await user_create_command.execute(user_schema=user_schema)


@router.get(
    "/me",
    responses={
        status.HTTP_200_OK: {"model": UserSchema},
        status.HTTP_401_UNAUTHORIZED: {"model": None},
    },
)
async def users_me(
    user: User = Depends(get_current_user),
) -> UserSchema:
    return UserSchema.from_orm(user)


@router.get(
    "/{user_id}",
    responses={
        status.HTTP_200_OK: {"model": UserSchema},
        status.HTTP_404_NOT_FOUND: {"model": APIException.Schema},
    },
)
async def users_retrieve(
    user: User = Depends(entity_getter(User, name="user_id")),
) -> UserSchema:
    return UserSchema.from_orm(user)


@router.get(
    "",
    responses={
        status.HTTP_200_OK: {"model": list[UserSchema]},
    },
)
@inject
async def users_root(
    session: Annotated[AsyncSession, Inject],
    ids: conset(uuid.UUID, max_items=1_000) | None = Query(None),  # type: ignore
) -> list[UserSchema]:
    stmt = select(User)
    if ids:
        stmt = stmt.where(User.id.in_(ids))
    users = await session.scalars(stmt)
    return pydantic.parse_obj_as(list[UserSchema], list(users))
