import uuid

from fastapi import Depends
from passlib.context import CryptContext
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from apps.users.dto import UserCreateDto
from apps.users.exceptions import UserAlreadyExists
from db.dependencies import get_session
from db.models import User
from dependencies import get_crypt_context


class UserService:
    def __init__(
        self,
        session: AsyncSession = Depends(get_session),
        crypt_context: CryptContext = Depends(get_crypt_context),
    ):
        self._session = session
        self._crypt_context = crypt_context

    async def get_user(
        self,
        entity_id: uuid.UUID | None = None,
        username: str | None = None,
    ) -> User | None:
        stmt = select(User).limit(1)
        if entity_id:
            stmt = stmt.where(User.id == entity_id)
        if username:
            stmt = stmt.where(User.username == username)

        user: User | None = await self._session.scalar(stmt)
        return user

    async def create_user(self, dto: UserCreateDto) -> User:
        if await self._session.scalar(
            select(User).where(User.username == dto.username)
        ):
            raise UserAlreadyExists

        user = User(
            username=dto.username,
            password_hash=self._crypt_context.hash(dto.password.get_secret_value()),
        )
        self._session.add(user)
        await self._session.flush()
        await self._session.refresh(user)
        return user
