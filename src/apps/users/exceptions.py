from exceptions import EntityAlreadyExists


class UserAlreadyExists(EntityAlreadyExists):
    pass
