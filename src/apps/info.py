from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter

from schema import BaseSchema
from settings import AuthSettings

router = APIRouter()


class InfoSchema(BaseSchema):
    class Config:
        title = "Info"

    public_key: str


@router.get("", response_model=InfoSchema)
@inject
async def info(
    auth_settings: Annotated[AuthSettings, Inject],
) -> InfoSchema:
    return InfoSchema(
        public_key=auth_settings.jwt_public_key,
    )
