import datetime

from pydantic import BaseSettings


class DatabaseSettings(BaseSettings):
    class Config:
        env_prefix = "database_"

    driver: str = "postgresql+asyncpg"
    database: str = "database"
    username: str = "postgres"
    password: str = "password"
    host: str = "postgres"

    echo: bool = False

    @property
    def url(self) -> str:
        return f"{self.driver}://{self.username}:{self.password}@{self.host}/{self.database}"

    @property
    def alembic_url(self) -> str:
        return self.url.replace("+asyncpg", "")


class AuthSettings(BaseSettings):
    class Config:
        env_prefix = "auth_"

    jwt_private_key: str
    jwt_public_key: str
    jwt_algorithm: str = "RS256"

    access_token_lifetime: datetime.timedelta = datetime.timedelta(minutes=15)
    refresh_token_lifetime: datetime.timedelta = datetime.timedelta(days=30)
    hashing_schemes: list[str] = ["argon2"]


class AppSettings(BaseSettings):
    domain: str = "http://identity.localhost"
