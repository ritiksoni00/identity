import functools
import inspect
import uuid
from functools import lru_cache
from typing import Annotated, Any, Awaitable, Callable, TypeVar

from aioinject import Inject
from aioinject.ext.fastapi import inject
from common.settings import get_settings
from common.web.exceptions import NotFound
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession

from settings import AuthSettings


@lru_cache
def get_crypt_context() -> CryptContext:
    auth_settings = get_settings(AuthSettings)
    return CryptContext(
        default=auth_settings.hashing_schemes[0],
        schemes=auth_settings.hashing_schemes,
    )


TModel = TypeVar("TModel")


@functools.lru_cache
def entity_getter(
    model: TModel,
    *,
    name: str,
    type_: type = uuid.UUID,
    is_optional: bool = False,
) -> Callable[..., Awaitable[TModel | None]]:
    async def _inner(
        session: Annotated[AsyncSession, Inject], **kwargs: Any
    ) -> TModel | None:
        entity_id = kwargs[name]
        entity: TModel | None = await session.get(model, entity_id)
        if entity is None and not is_optional:
            raise NotFound
        return entity

    signature = inspect.signature(_inner)

    _inner.__signature__ = signature.replace(  # type: ignore
        parameters=[
            inspect.Parameter(
                name=name,
                annotation=type_,
                kind=inspect.Parameter.KEYWORD_ONLY,
            ),
        ]
    )
    return inject(_inner)  # type: ignore
